import 'package:flutter/material.dart';
import 'package:flutter_foonkie_monkey_app/features/home/presentation/pages/foonkie_monkey_page.dart';

import '../features/home/presentation/views/main_view.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'main_view': (_)=>const MainView(),
  'foonkie_monkey_page': (_) => const FoonkieMonkeyPage()
};
