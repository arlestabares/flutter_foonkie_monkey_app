import 'package:flutter/material.dart';
import 'package:flutter_foonkie_monkey_app/features/blocs/blocs_provider.dart';
import 'package:flutter_foonkie_monkey_app/features/home/data/repositories/repository.dart';
import 'package:flutter_foonkie_monkey_app/features/home/data/repositories/repository_impl.dart';
import 'package:flutter_foonkie_monkey_app/features/repositories/repository_provider.dart';
import 'package:flutter_foonkie_monkey_app/routes/routes.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // final database =
  //     await $FloorAppDatabase.databaseBuilder('app_database.db').build();
  // final localDatasource = UserLocalDataSource();
  // await localDatasource.init();
  // final userdao = database.userDao;
  final repository = RepositoryImpl();
  repository.init();
  runApp(MyApp(
    repository: repository,
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({
    Key? key,
    required this.repository,
  }) : super(key: key);
  final Repository repository;
  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProviderWidget(
      repository: repository,
      child: MultiBlocProviderWidget(
        child: MaterialApp(
          title: 'Material App',
          // initialRoute: 'foonkie_monkey_page',
          initialRoute: 'main_view',
          routes: appRoutes,
        ),
      ),
    );
  }
}
