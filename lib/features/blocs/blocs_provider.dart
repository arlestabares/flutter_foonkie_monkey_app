import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart' as base;
import 'package:flutter_foonkie_monkey_app/features/home/presentation/bloc/bloc.dart';

class MultiBlocProviderWidget extends StatelessWidget {
  const MultiBlocProviderWidget({Key? key, required this.child})
      : super(key: key);
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return base.MultiBlocProvider(
      providers: [base.BlocProvider(create: (context) => HomeBloc())],
      child: child,
    );
  }
}
