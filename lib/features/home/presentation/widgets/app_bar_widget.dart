import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_foonkie_monkey_app/features/home/presentation/bloc/bloc.dart';

AppBar appbarUserDetails(BuildContext context) {
  return AppBar(
    elevation: 0.0,
    backgroundColor: Colors.white,
    leading: IconButton(
      onPressed: () {
        context.read<HomeBloc>().add(const UserDatabaseEvent(true));
        Navigator.pop(context);
      },
      icon: const Icon(
        Icons.arrow_back_ios,
        color: Colors.black,
      ),
    ),
  );
}
