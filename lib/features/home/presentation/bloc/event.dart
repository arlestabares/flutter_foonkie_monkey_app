part of 'bloc.dart';

abstract class Event extends Equatable {
  const Event();

  @override
  List<Object> get props => [];
}

class InitialEvent extends Event {}

class UserLoadEvent extends Event {}

class GetAllUserRemoteEvent extends Event {}

class ShowAllUserRemoteEvent extends Event {}

class UserDatabaseEvent extends Event {
  final bool isUserLoad;

  const UserDatabaseEvent(this.isUserLoad);
  // final List<User> userList;
  // const AddUserDatabaseEvent(this.userList);
}

class UserDetailsUploadEvent extends Event {
  final User user;

  const UserDetailsUploadEvent(this.user);
}

class ShowDeviceGalleryEvent extends Event {}
