import 'package:bloc/bloc.dart' as bloc;
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_foonkie_monkey_app/features/home/data/models/user_model.dart';
import 'package:flutter_foonkie_monkey_app/features/home/data/repositories/repository_impl.dart';

part 'event.dart';
part 'state.dart';

class HomeBloc extends bloc.Bloc<Event, HomeState> {
  final RepositoryImpl repository;
  HomeBloc([RepositoryImpl? repository])
      : repository = repository ?? RepositoryImpl(),
        super(initialState) {
    on<GetAllUserRemoteEvent>(_onGetAllUserRemote);
    on<ShowAllUserRemoteEvent>(_onShowAllUserRemote);
    on<UserDetailsUploadEvent>(_onShowUserDetails);
    on<UserDatabaseEvent>(_onAddUserDatabase);
    on<ShowDeviceGalleryEvent>(_onShowDeviceGallery);
  }
  static HomeState get initialState => const InitialState(Model());

  _onGetAllUserRemote(
      GetAllUserRemoteEvent event, Emitter<HomeState> emit) async {
    emit(InitialState(state.model));
    List<User> userListResponse = [];

    userListResponse = await repository.getData();
    emit(GetAllUserRemoteState(
        state.model.copyWith(usersList: userListResponse)));
    
  }

  _onShowAllUserRemote(ShowAllUserRemoteEvent event, Emitter<HomeState> emit) {
    emit(InitialState(state.model));
    emit(
      ShowAllUserRemoteState(
        state.model.copyWith(usersList: state.model.usersList),
      ),
    );
  }

  _onShowUserDetails(
      UserDetailsUploadEvent event, Emitter<HomeState> emit) async {
    emit(InitialState(state.model));
    final usuario = await repository.findUserById(event.user.id!);

    emit(UserDetailsUploadState(state.model.copyWith(user: usuario)));
  }

  _onAddUserDatabase(UserDatabaseEvent event, Emitter<HomeState> emit) async {
    emit(InitialState(state.model));
    final lista = state.model.usersList!;
    await repository.insertUserList(lista);
    // final showUser = await repository.findAllUser();

    emit(
      UsersDatabaseState(
        state.model.copyWith(usersList: lista, isUserLoad: event.isUserLoad),
      ),
    );
    emit(GetAllUserRemoteState(
        state.model.copyWith(isUserLoad: event.isUserLoad)));
  }

  _onShowDeviceGallery(ShowDeviceGalleryEvent event, Emitter<HomeState> emit) {
    emit(InitialState(state.model));
    emit(ShowDeviceGalleryState(state.model.copyWith()));
  }
}
