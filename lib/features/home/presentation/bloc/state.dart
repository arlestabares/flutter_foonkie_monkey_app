part of 'bloc.dart';

abstract class HomeState extends Equatable {
  const HomeState(this.model);
  final Model model;

  @override
  List<Object> get props => [];
}

class InitialState extends HomeState {
  const InitialState(Model model) : super(model);
}

class UserLoadState extends HomeState {
  const UserLoadState(Model model) : super(model);
}

class GetAllUserRemoteState extends HomeState {
  const GetAllUserRemoteState(Model model) : super(model);
}

class ShowAllUserRemoteState extends HomeState {
  const ShowAllUserRemoteState(Model model) : super(model);
}

class UsersDatabaseState extends HomeState {
  const UsersDatabaseState(Model model) : super(model);
}

class UserDetailsUploadState extends HomeState {
  const UserDetailsUploadState(Model model) : super(model);
}

class ShowDeviceGalleryState extends HomeState {
  const ShowDeviceGalleryState(Model model) : super(model);
}

class Model extends Equatable {
  final User? user;
  final List<User>? usersList;

  final bool isUserLoad;
  const Model({
    this.isUserLoad = false,
    this.user,
    this.usersList,
  });

  Model copyWith({
    int? page,
    User? user,
    bool? isUserLoad,
    List<User>? usersList,
  }) {
    return Model(
      user: user ?? this.user,
      isUserLoad: isUserLoad ?? this.isUserLoad,
      usersList: usersList ?? this.usersList,
    );
  }

  @override
  List<Object?> get props => [user, usersList, isUserLoad];
}
