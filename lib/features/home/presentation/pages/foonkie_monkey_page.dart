import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_foonkie_monkey_app/features/home/presentation/bloc/bloc.dart';
import 'package:flutter_foonkie_monkey_app/features/home/presentation/views/user_list_view.dart';

class FoonkieMonkeyPage extends StatelessWidget {
  const FoonkieMonkeyPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: ElevatedButton(
              onPressed: () {
                // user.getData();
                context.read<HomeBloc>().add(
                      GetAllUserRemoteEvent(),
                    );
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const UserListView(),
                  ),
                );
              },
              child: const Text("TraerData"),
            ),
          ),
          const SizedBox(height: 21.0),
          ElevatedButton(
            onPressed: () {
              BlocProvider.of<HomeBloc>(context).add(
                ShowAllUserRemoteEvent(),
              );
            },
            child: const Text("Lista de usuarios"),
          ),
        ],
      ),
    );
  }
}
