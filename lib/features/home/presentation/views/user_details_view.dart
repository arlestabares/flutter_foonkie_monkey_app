import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_foonkie_monkey_app/features/home/presentation/bloc/bloc.dart';
import 'package:image_picker/image_picker.dart';

import '../widgets/app_bar_widget.dart';

class UserDetailsView extends StatelessWidget {
  const UserDetailsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appbarUserDetails(context),
      body: SafeArea(
        child: Container(
          color: Colors.white,
          padding: const EdgeInsets.all(2.0),
          child: Column(
            children: const [
              UserDetails(),
            ],
          ),
        ),
      ),
    );
  }
}

class UserDetails extends StatefulWidget {
  const UserDetails({Key? key}) : super(key: key);

  @override
  State<UserDetails> createState() => _UserDetailsState();
}

class _UserDetailsState extends State<UserDetails> {
  bool isImageLoad = false;
  File? image;
  final picker = ImagePicker();
  Future selectedImage(op) async {
    var pickedFile; //sera el archivo seleccionado
    if (op == 1) {
      pickedFile = await picker.pickImage(source: ImageSource.camera);
    } else {
      pickedFile = await picker.pickImage(source: ImageSource.gallery);
    }
    setState(() {
      if (pickedFile != null) {
        image = File(pickedFile.path);
        isImageLoad = true;
      } else {}
    });
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final style = TextStyle(fontSize: size.width > 380? 21.0: 16.0, fontWeight: FontWeight.bold);
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (context, state) {
        if (state is UserDetailsUploadState) {
          return Container(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              children: [
                Stack(
                  children: [
                    BlocBuilder<HomeBloc, HomeState>(
                      builder: (context, state) {
                        if (state is UserDetailsUploadState) {
                          return !isImageLoad
                              ? Container(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                      fit: BoxFit.contain,
                                      image: NetworkImage(
                                        state.model.user!.avatar!,
                                      ),
                                    ),
                                  ),
                                  height: 250.0,
                                )
                              : Container(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: FileImage(image!),
                                    ),
                                  ),
                                  height: 250.0,
                                );
                        }
                        return const Center(child: CircularProgressIndicator());
                      },
                    ),
                    Positioned(
                      right: 100.0,
                      top: 200.0,
                      child: CircleAvatar(
                        radius: 25.0,
                        child: IconButton(
                          onPressed: () {
                            showModalBottomSheet<void>(
                              context: context,
                              builder: (BuildContext context) {
                                return Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 12.0),
                                  height: 230,
                                  child: Center(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: const [
                                            Text('Foto del perfil')
                                          ],
                                        ),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Column(
                                              children: [
                                                Container(
                                                  padding: const EdgeInsets.all(
                                                      12.0),
                                                  child: CircleAvatar(
                                                    radius: 25.0,
                                                    child: IconButton(
                                                      onPressed: () {
                                                        setState(() {});
                                                        selectedImage(1);
                                                      },
                                                      icon: const Icon(
                                                        Icons
                                                            .camera_alt_outlined,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                const Text("Camara")
                                              ],
                                            ),
                                            const SizedBox(width: 21.0),
                                            Column(
                                              children: [
                                                Container(
                                                  padding: const EdgeInsets.all(
                                                    12.0,
                                                  ),
                                                  child: CircleAvatar(
                                                    radius: 25.0,
                                                    child: IconButton(
                                                      onPressed: () {
                                                        setState(() {});
                                                        selectedImage(2);
                                                      },
                                                      icon: const Icon(
                                                        Icons.image,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                const Text("Galeria")
                                              ],
                                            ),
                                          ],
                                        ),
                                        ElevatedButton(
                                          child: const Text('Close'),
                                          onPressed: () =>
                                              Navigator.pop(context),
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              },
                            );
                          },
                          icon: const Icon(Icons.camera_alt_outlined),
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 41.0),
                Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          height: 50.0,
                          width: 50.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              fit: BoxFit.contain,
                              image: NetworkImage(state.model.user!.avatar!),
                            ),
                          ),
                        ),
                        const SizedBox(width: 21.0),
                        Expanded(
                          child: ListTile(
                            title: Text(
                              '${state.model.user!.firstName} ${state.model.user!.lastName}',
                              style: style,
                            ),
                            subtitle: Text(
                              state.model.user!.email!,
                              style: style,
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
          );
        }
        return const Center(child: CircularProgressIndicator());
      },
    );
  }
}

// class UserImage extends StatelessWidget {
//   const UserImage({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return BlocBuilder<HomeBloc, HomeState>(
//       builder: (context, state) {
//         if (state is UserDetailsUploadState) {
//           return Container(
//             decoration: BoxDecoration(
//               shape: BoxShape.circle,
//               image: DecorationImage(
//                 fit: BoxFit.contain,
//                 image: NetworkImage(state.model.user!.avatar!),
//               ),
//             ),
//             height: 250.0,
//           );
//         }
//         return const Center(child: CircularProgressIndicator());
//       },
//     );
//   }
// }

// class CamaraOption extends StatelessWidget {
//   const CamaraOption({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Positioned(
//       right: 100.0,
//       top: 200.0,
//       child: CircleAvatar(
//         radius: 25.0,
//         child: IconButton(
//           onPressed: () {
//             showModalBottomSheet<void>(
//               context: context,
//               builder: (BuildContext context) {
//                 return Container(
//                   padding: const EdgeInsets.symmetric(horizontal: 12.0),
//                   height: 230,
//                   child: Center(
//                     child: Column(
//                       mainAxisAlignment: MainAxisAlignment.start,
//                       mainAxisSize: MainAxisSize.min,
//                       children: <Widget>[
//                         Row(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: const [Text('Foto del perfil')],
//                         ),
//                         Row(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: [
//                             Column(
//                               children: [
//                                 Container(
//                                   padding: const EdgeInsets.all(12.0),
//                                   child: CircleAvatar(
//                                     radius: 25.0,
//                                     child: IconButton(
//                                       onPressed: () async {},
//                                       icon:
//                                           const Icon(Icons.camera_alt_outlined),
//                                     ),
//                                   ),
//                                 ),
//                                 const Text("Camara")
//                               ],
//                             ),
//                             const SizedBox(width: 21.0),
//                             Column(
//                               children: [
//                                 Container(
//                                   padding: const EdgeInsets.all(12.0),
//                                   child: CircleAvatar(
//                                     radius: 25.0,
//                                     child: IconButton(
//                                       onPressed: () {},
//                                       icon: const Icon(Icons.image),
//                                     ),
//                                   ),
//                                 ),
//                                 const Text("Galeria")
//                               ],
//                             ),
//                           ],
//                         ),
//                         ElevatedButton(
//                           child: const Text('Close'),
//                           onPressed: () => Navigator.pop(context),
//                         )
//                       ],
//                     ),
//                   ),
//                 );
//               },
//             );
//           },
//           icon: const Icon(Icons.camera_alt_outlined),
//         ),
//       ),
//     );
//   }
// }
