import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_foonkie_monkey_app/features/home/presentation/bloc/bloc.dart';
import 'package:flutter_foonkie_monkey_app/features/home/presentation/views/category_list.dart';
import 'package:flutter_foonkie_monkey_app/features/home/presentation/views/user_list_view.dart';
import 'package:flutter_foonkie_monkey_app/features/utils/string.dart';
import 'package:flutter_foonkie_monkey_app/features/utils/style_button.dart';
import 'package:flutter_foonkie_monkey_app/features/utils/text_button.dart';

import '../../../utils/send_email.dart';

class MainView extends StatelessWidget {
  const MainView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: ScreenList(),
    );
  }
}

class ScreenList extends StatelessWidget {
  const ScreenList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    print('===========${size.width}');
    return Container(
      color: const Color(0xFFB3DDD8),
      child: Column(
        children: [
          Expanded(
            child: ListView(
              children: [
                Stack(
                  alignment: Alignment.topCenter,
                  children: [
                    Container(
                      height: size.height,
                      decoration: const BoxDecoration(color: Color(0xFFB3DDD8)),
                    ),
                    Column(
                      children: [
                        Container(
                          height: 120.0,
                          padding: const EdgeInsets.only(top: 31.0),
                          child: Image.asset('assets/icons/FoonkieMonkey.png'),
                        ),
                        const Text(text1,
                            style: style3, textAlign: TextAlign.center),
                        const SizedBox(height: 21.0),
                        const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 18.0),
                          child: Text(text2,
                              style: style2, textAlign: TextAlign.center),
                        ),
                        const SizedBox(height: 21.0),
                        ElevatedButton(
                          style: outLinedButtonStyle,
                          child: const Text(textButton, style: style1),
                          onPressed: () async {
                            await sendEmail();
                          },
                        ),
                        const SizedBox(height: 21.0),
                        Container(
                          height: 750.0,
                          decoration: const BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage('assets/icons/Monkey-1.png'),
                              fit: BoxFit.cover,
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
                Stack(
                  children: [
                    Container(
                      height: size.height,
                      decoration: const BoxDecoration(color: Color(0xFFAAAACC)),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 61.0, vertical: 21.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            textTitle,
                            style: style4,
                          ),
                          Container(
                              height: 2.0, color: Colors.white30, width: 91.0),
                          const SizedBox(height: 21.0),
                          Container(
                            // height: size.height * 0.66,
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: const Color(0xFFE6E3EF),
                              borderRadius: BorderRadius.circular(31.0),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 310.0,
                                  child: Image.asset(
                                    'assets/icons/pfizer-motion.png',
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 18.0,
                                    vertical: 12.0,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                       Text(textTitle2, style:  TextStyle(fontSize: size.width >380? 31.0: 21.0, fontWeight: FontWeight.bold, color: Colors.black)),
                                      Container(
                                        height: 2.0,
                                        color: Colors.grey,
                                        width: 70.0,
                                      ),
                                      const SizedBox(height: 16.0),
                                      const Text(textTitle3, style: style),
                                      const SizedBox(height: 21.0),
                                      ElevatedButton(
                                        style: outLinedButtonStyle,
                                        child: const Text(
                                          textButton2,
                                          style: style1,
                                        ),
                                        onPressed: () {
                                          context.read<HomeBloc>().add(
                                                GetAllUserRemoteEvent(),
                                              );
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  const UserListView(),
                                            ),
                                          );
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const CategoryList()
                        ],
                      ),
                    ),
                  ],
                ),
                Stack(
                  children: [
                    Container(
                      height: size.height * 0.95,
                      decoration: const BoxDecoration(
                        color: Color(0xFF29ABE1),
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Image.asset(
                                'assets/icons/Monkey-2.png',
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(textTitle4, style: style4),
                                Text(
                                  'Button',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline4!
                                      .copyWith(),
                                ),
                                Container(
                                  padding: const EdgeInsets.only(right: 21.0),
                                  child: ElevatedButton(
                                    style: outLinedButtonStyle,
                                    child: const Text(
                                      textButton,
                                      style: style1,
                                    ),
                                    onPressed: () {
                                      sendEmail();
                                    },
                                  ),
                                ),
                                const SizedBox(height: 12.0),
                                Container(
                                  height: 2.0,
                                  color: Colors.white54,
                                  width: 51.0,
                                )
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(height: 91.0),
                        //  const Spacer(),
                        Container(
                          height: 100.0,
                          padding: const EdgeInsets.only(right: 31.0),
                          child: Image.asset('assets/icons/FoonkieMonkey.png'),
                        ),
                        Container(
                          padding: const EdgeInsets.only(right: 31.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                'Bogotá',
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .copyWith(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                              ),
                              Text(
                                'Calle 106# 54-15 of. 307/308',
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .copyWith(
                                        color: Colors.white.withOpacity(0.9)),
                              ),
                              const SizedBox(height: 21.0),
                              Text(
                                'Harpenden, UK',
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .copyWith(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                              ),
                              Text(
                                'Harpenden Hall, Southdown Rd',
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .copyWith(
                                        color: Colors.white.withOpacity(0.9)),
                              ),
                              const SizedBox(height: 21.0),
                              Text(
                                'Miami, USA',
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .copyWith(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                              ),
                              Text(
                                '990 Biscayne Blvd #501',
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .copyWith(
                                        color: Colors.white.withOpacity(0.9)),
                              ),
                              const SizedBox(height: 21.0),
                              Container(
                                height: 2.0,
                                color: Colors.white54,
                                width: 51.0,
                              ),
                              const SizedBox(height: 21.0),
                              Text(
                                'Foonkie Monkey 2021',
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .copyWith(
                                      color: Colors.white.withOpacity(0.7),
                                      fontWeight: FontWeight.bold,
                                    ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
