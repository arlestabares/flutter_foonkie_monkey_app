import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_foonkie_monkey_app/features/home/presentation/views/user_details_view.dart';

import '../bloc/bloc.dart';
import '../widgets/appbar_user_list.dart';

class UserListView extends StatelessWidget {
  const UserListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarUserList(context),
      body: Container(
        color: Colors.white,
        padding: const EdgeInsets.all(8.0),
        child: BlocBuilder<HomeBloc, HomeState>(
          buildWhen: (_, state) => state is GetAllUserRemoteState,
          builder: (context, state) {
            if (state is GetAllUserRemoteState || state is UsersDatabaseState) {
              return ListView.builder(
                itemCount: state.model.usersList?.length,
                itemBuilder: (context, int index) {
                  final user = state.model.usersList?[index];
                  return Card(
                    child: InkWell(
                      onTap: () {
                        if (!state.model.isUserLoad) {
                          showDialog<String>(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                              title: const Text('Guardar datos'),
                              content: const Text(
                                  'Debe guardar los datos \npara ver el detalle'),
                              actions: <Widget>[
                                Center(
                                  child: TextButton(
                                    onPressed: () =>
                                        Navigator.pop(context, 'OK'),
                                    child: const Text('OK'),
                                  ),
                                ),
                              ],
                            ),
                          );
                        } else {
                          context
                              .read<HomeBloc>()
                              .add(UserDetailsUploadEvent(user!));
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => const UserDetailsView(),
                            ),
                          );
                        }
                      },
                      child: ListTile(
                        leading: Container(
                          decoration:
                              const BoxDecoration(shape: BoxShape.circle),
                          width: 80.0,
                          height: 90.0,
                          child: FadeInImage.assetNetwork(
                            placeholder: 'assets/images/no-image.jpg',
                            image: user!.avatar!,
                            width: 50.0,
                            height: 80.0,
                            fit: BoxFit.contain,
                          ),
                        ),
                        title: Text(
                          '${user.firstName!} ${user.lastName!}',
                          style: const TextStyle(
                            fontSize: 21.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        subtitle: Text(
                          user.email!,
                          style: const TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.w400,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                  );
                },
              );
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog<String>(
            context: context,
            builder: (BuildContext context) => AlertDialog(
              title: const Text('Base de datos'),
              content: const Text('Los datos han sido Guardados'),
              actions: <Widget>[
                Center(
                  child: TextButton(
                    onPressed: () => Navigator.pop(context, 'OK'),
                    child: const Text(
                      'OK',
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                ),
              ],
            ),
          );
          context.read<HomeBloc>().add(const UserDatabaseEvent(true));
        },
        child: const Icon(Icons.add_task),
      ),
    );
  }
}

// class UserDetailsBlocBuilder extends StatelessWidget {
//   const UserDetailsBlocBuilder({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       color: Colors.blue,
//       height: 400.0,
//       child: BlocBuilder<HomeBloc, HomeState>(
//         buildWhen: (_, state) => state is GetAllUserRemoteState,
//         builder: (context, state) {
//           if (state is GetAllUserRemoteState) {
//             return ListView.builder(
//               itemCount: state.model.usersList?.length,
//               itemBuilder: (context, int index) {
//                 final user = state.model.usersList?[index];
//                 return Card(
//                   child: InkWell(
//                     onTap: () {
//                       if (!state.model.isUserLoad) {
//                         showDialog<String>(
//                           context: context,
//                           builder: (BuildContext context) => AlertDialog(
//                             title: const Text('Guardar datos'),
//                             content: const Text(
//                                 'Debe guardar los datos \npara ver el detalle'),
//                             actions: <Widget>[
//                               Center(
//                                 child: TextButton(
//                                   onPressed: () => Navigator.pop(context, 'OK'),
//                                   child: const Text('OK'),
//                                 ),
//                               ),
//                             ],
//                           ),
//                         );
//                       } else {
//                         context
//                             .read<HomeBloc>()
//                             .add(UserDetailsUploadEvent(user!));
//                         Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) => const UserDetailsView(),
//                           ),
//                         );
//                       }
//                     },
//                     child: ListTile(
//                       leading: Container(
//                         decoration: const BoxDecoration(shape: BoxShape.circle),
//                         width: 80.0,
//                         height: 90.0,
//                         child: FadeInImage.assetNetwork(
//                           placeholder: 'assets/images/no-image.jpg',
//                           image: user!.avatar!,
//                           width: 50.0,
//                           height: 80.0,
//                           fit: BoxFit.contain,
//                         ),
//                       ),
//                       title: Text(
//                         '${user.firstName!} ${user.lastName!}',
//                         style: const TextStyle(
//                           fontSize: 21.0,
//                           fontWeight: FontWeight.bold,
//                         ),
//                       ),
//                       subtitle: Text(
//                         user.email!,
//                         style: const TextStyle(
//                           fontSize: 18.0,
//                           fontWeight: FontWeight.w400,
//                           color: Colors.black,
//                         ),
//                       ),
//                     ),
//                   ),
//                 );
//               },
//             );
//           }
//           return const Center(
//             child: CircularProgressIndicator(),
//           );
//         },
//       ),
//     );
//   }
// }
