import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqlbrite/sqlbrite.dart';
import 'package:synchronized/synchronized.dart';

import '../../models/user_model.dart';

class DatabaseHelper {
  static const _databaseName = 'Users.db';
  static const _databaseVersion = 1;

  static const userTable = 'User';
  static const userId = 'userId';

  static late BriteDatabase _streamDatabase;

  DatabaseHelper.privateConstructor();

  static final DatabaseHelper instance = DatabaseHelper.privateConstructor();
//evita el acceso simultaneo a la db
  static var lock = Lock();
//instancia privada de la db
  static Database? _database;

  Future _onCreate(Database db, int version) async {
    await db.execute('''
        CREATE TABLE $userTable (
          id INTEGER PRIMARY KEY, 
          email TEXT,
          first_name TEXT,
          last_name TEXT,
          avatar REAL 
        )
        ''');
  }

  Future<Database> _initDatabase() async {
    final documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, _databaseName);
    Sqflite.setDebugModeOn(true);
    return openDatabase(path, version: _databaseVersion, onCreate: _onCreate);
  }

  Future<Database> get getDatabase async {
    if (_database != null) return _database!;
    //Objeto para evitar el acceso simultáneo a los datos
    await lock.synchronized(
      () async {
        //crea una instancia perezosa de la base de datos la primera vez
        //que se accede a ella
        if (_database == null) {
          _database = await _initDatabase();
          _streamDatabase = BriteDatabase(_database!);
        }
      },
    );
    return _database!;
  }

  Future<BriteDatabase> get streamDatabase async {
    await getDatabase;
    return _streamDatabase;
  }

  Future<int> insert(String table, Map<String, dynamic> row) async {
    final db = await instance.getDatabase;
    return db.insert(table, row, conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<int> insertUser(User user) async {
    return insert(userTable, user.toJson());
  }

  Future<User> findUserById(int id) async {
    final db = await instance.streamDatabase;
    final recipeList = await db.query(userTable, where: 'id = $id');
    final recipes = parseUser(recipeList);
    return recipes.first;
  }

  Future<int> insertUserList(User user) async {
    return insert(userTable, user.toJson());
  }

  Future<List<User>> findAllUsers() async {
    final db = await instance.getDatabase;
    final productsMapList = await db.query(userTable);
    final usuariosList = parseUsers(productsMapList);
    return usuariosList;
  }

  List<User> parseUsers(List<Map<String, dynamic>> usuariosMapList) {
    final usuariosList = <User>[];
    for (var usuarioMap in usuariosMapList) {
      final usuario = User.fromJson(usuarioMap);
      usuariosList.add(usuario);
    }
    return usuariosList;
  }

  List<User> parseUser(List<Map<String, dynamic>> recipeList) {
    final recipes = <User>[];
    for (var recipeMap in recipeList) {
      final recipe = User.fromJson(recipeMap);
      recipes.add(recipe);
    }
    return recipes;
  }

  void close() {
    _database?.close();
  }
}
