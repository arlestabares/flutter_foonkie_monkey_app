import 'dart:convert';

import 'package:flutter_foonkie_monkey_app/features/home/data/network/url_paths.dart';

import '../../models/user_model.dart';
import 'package:http/http.dart' as http;

abstract class IUserRemoteDataSource<T> {
  Future<T> getData();
}

class UserRemoteDataSource implements IUserRemoteDataSource {
  int userCountPage = 1;

  @override
  Future<List<User>> getData() async {
    // final uri = Uri.https(UrlPaths.host, UrlPaths.path, {
    //   'page': userCountPage.toString(),
    // });
    final response = await _processdata(userCountPage);
    userCountPage++;
    List<User> temp = [];
    for (var element in response) {
      temp.add(element);
    }
    if (userCountPage <= 2) {
      final response2 = await _processdata(userCountPage++);
      for (var element in response2) {
        temp.add(element);
      }
      userCountPage = 1;
    }
    return temp;
  }

  Future<List<User>> _processdata(int userCountPage) async {
    final uri = Uri.https(UrlPaths.host, UrlPaths.path, {
      'page': userCountPage.toString(),
    });
    try {
      final res = await http.get(uri);
      final decodeData = json.decode(res.body);
      final userList = UserList.fromJson(decodeData['data']);
      return userList.items;
    } catch (e) {
      return List.empty();
    }
  }
}
