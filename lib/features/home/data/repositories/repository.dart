import 'package:flutter_foonkie_monkey_app/features/home/data/models/user_model.dart';

abstract class Repository{
Future<List<User>> findAllUser();
Future<User?> findUserById(int id);
// Stream<User> whatAllUser();
Future<List<int>> insertUserList(List<User> userList); 
Future<void> insertUser(User user);
  Future init();
  void close();
}