import 'package:flutter_foonkie_monkey_app/features/home/data/models/user_model.dart';
import 'package:flutter_foonkie_monkey_app/features/home/data/repositories/repository.dart';

import '../datasources/remote/user_remote.dart';
import '../datasources/sqlite/database_helper.dart';

class RepositoryImpl implements Repository {
  final dbHelper = DatabaseHelper.instance;
  UserRemoteDataSource remoteDataSource = UserRemoteDataSource();

  RepositoryImpl();

  Future<List<User>> getData() async {
    return await remoteDataSource.getData();
  }

  @override
  Future<List<User>> findAllUser() async {
    return await dbHelper.findAllUsers();
  }

  @override
  Future<User?> findUserById(int id) async {
    return await dbHelper.findUserById(id);
  }

  @override
  Future<int> insertUser(User user) async {
    return await dbHelper.insertUser(user);
  }

  @override
  Future<List<int>> insertUserList(List<User> userList) {
    List<int> userlistId = <int>[];
    return Future(() async {
      if (userList.isNotEmpty) {
         userlistId = <int>[];
        await Future.forEach(userList, (User user) async {
          final futureId = await dbHelper.insertUserList(user);
          user.id = futureId;
          userlistId.add(futureId);
        });
        return Future.value(userlistId);
      } else {
        Future.value(<int>[]);
      }
      return Future.value(<int>[]);
      // throw ('Algo salio mal');
    });
  }

  @override
  void close() {
    dbHelper.close();
  }

  @override
  Future init() async {
    await dbHelper.getDatabase;
    return Future.value();
  }
}
