 import 'package:url_launcher/url_launcher.dart';

String? encodeQueryParameters(Map<String, String> params) {
    return params.entries
        .map((MapEntry<String, String> e) =>
            '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
        .join('&');
  }

  Future<void> sendEmail() async {
    final Uri emailLaunchUri = Uri(
      scheme: 'mailto',
      path: 'test@example.com',
      query: encodeQueryParameters(<String, String>{
        'subject': 'I want a quote',
        'body': 'I need you to build an application'
      }),
    );
    launchUrl(emailLaunchUri);
  }