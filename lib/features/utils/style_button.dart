 import 'package:flutter/material.dart';

final ButtonStyle outLinedButtonStyle = OutlinedButton.styleFrom(
  primary: Colors.white,
  backgroundColor: Colors.white,
  minimumSize:const Size(0, 36),
  padding:const EdgeInsets.symmetric(horizontal: 16.0),
  shape:const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(24)),
  ),
);