import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart' as base;

import '../home/data/repositories/repository.dart';

class MultiRepositoryProviderWidget extends StatelessWidget {
  const MultiRepositoryProviderWidget(
      {Key? key, required this.child, required this.repository})
      : super(key: key);
  final Widget child;
  final Repository repository;
  @override
  Widget build(BuildContext context) {
    return base.MultiRepositoryProvider(
      providers: [
        base.RepositoryProvider<Repository>(
          lazy: false,
          create: (context) => repository,
        )
      ],
      child: child,
    );
  }
}
